#!/usr/bin/env node

"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
var setup_account_1 = require("./tasks/setup-account");
var setup_auth_1 = require("./tasks/setup-auth");
var setup_cleanup_1 = require("./tasks/setup-cleanup");
var setup_codegen_1 = require("./tasks/setup-codegen");
var setup_colors_1 = require("./tasks/setup-colors");
var setup_env_1 = require("./tasks/setup-env");
var setup_git_1 = require("./tasks/setup-git");
var setup_logo_1 = require("./tasks/setup-logo");
var setup_metadata_1 = require("./tasks/setup-metadata");
var setup_node_1 = require("./tasks/setup-node");
var setup_template_1 = require("./tasks/setup-template");
var setup_title_1 = require("./tasks/setup-title");
var setup = function () { return __awaiter(void 0, void 0, void 0, function () {
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0: return [4 /*yield*/, setup_template_1.setupTemplate()];
            case 1:
                _a.sent();
                return [4 /*yield*/, setup_git_1.setupGit()];
            case 2:
                _a.sent();
                return [4 /*yield*/, setup_title_1.setupTitle()];
            case 3:
                _a.sent();
                return [4 /*yield*/, setup_logo_1.setupLogo()];
            case 4:
                _a.sent();
                return [4 /*yield*/, setup_metadata_1.setupMetadata()];
            case 5:
                _a.sent();
                return [4 /*yield*/, setup_env_1.setupEnv()];
            case 6:
                _a.sent();
                return [4 /*yield*/, setup_auth_1.setupAuth()];
            case 7:
                _a.sent();
                return [4 /*yield*/, setup_account_1.setupAccount()];
            case 8:
                _a.sent();
                return [4 /*yield*/, setup_colors_1.setupColors()];
            case 9:
                _a.sent();
                return [4 /*yield*/, setup_node_1.setupNode()];
            case 10:
                _a.sent();
                return [4 /*yield*/, setup_codegen_1.setupCodegen()];
            case 11:
                _a.sent();
                return [4 /*yield*/, setup_cleanup_1.setupCleanup()];
            case 12:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); };
setup()
    .then(function () {
    process.exit(0);
})["catch"](function () {
    process.exit(1);
});
