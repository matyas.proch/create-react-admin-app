"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
exports.__esModule = true;
exports.writeToFile = exports.selectVersion = exports.skipKey = exports.replaceKey = exports.skipBlock = exports.replaceBlockWithAlternative = exports.replaceBlock = exports.execBatch = exports.exec = exports.info = exports.question = void 0;
var cp = require("child_process");
var fs = require("fs");
var path = require("path");
var readline = require("readline");
var rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});
var question = function (_a) {
    var prompt = _a.prompt, _b = _a.optional, optional = _b === void 0 ? true : _b;
    return __awaiter(void 0, void 0, void 0, function () {
        return __generator(this, function (_c) {
            return [2 /*return*/, new Promise(function (resolve) {
                    var ask = function () {
                        rl.question("\n" + prompt + "\n", function (answer) {
                            if (!optional && !answer) {
                                ask();
                            }
                            else {
                                resolve(answer);
                            }
                        });
                    };
                    ask();
                })];
        });
    });
};
exports.question = question;
var info = function (_a) {
    var message = _a.message;
    console.log(message);
};
exports.info = info;
var exec = function (_a) {
    var command = _a.command;
    exports.info({ message: cp.execSync(command).toString() });
};
exports.exec = exec;
var execBatch = function (_a) {
    var commands = _a.commands;
    commands.forEach(function (command) {
        cp.execSync(command);
    });
};
exports.execBatch = execBatch;
var applyForFile = function (_a) {
    var folderPath = _a.folderPath, callback = _a.callback;
    var files = fs.readdirSync(folderPath);
    files.forEach(function (file) {
        var stat = fs.lstatSync(path.join(folderPath, file));
        if (stat.isDirectory()) {
            applyForFile({ folderPath: path.join(folderPath, file), callback: callback });
            return;
        }
        if (stat.isFile()) {
            var extension = path.extname(path.join(folderPath, file));
            if (['.png', '.jpg', '.jpeg', '.gif', 'md'].includes(extension)) {
                return;
            }
            callback({ filePath: path.join(folderPath, file) });
        }
    });
};
var findAndReplace = function (_a) {
    var replaces = _a.replaces, value = _a.value;
    var callback = function (_a) {
        var filePath = _a.filePath;
        var content = fs.readFileSync(filePath).toString();
        var newContent = replaces.reduce(function (tmpContent, replace) { return tmpContent.replace(replace, value); }, content);
        fs.writeFileSync(filePath, newContent);
    };
    applyForFile({ folderPath: '.', callback: callback });
};
var replaceBlock = function (_a) {
    var blockName = _a.blockName, value = _a.value;
    var replaces = [
        new RegExp("# BLOCK-START:" + blockName + "(.|[\r\n])*# BLOCK-END:" + blockName, 'g'),
        new RegExp("<-- BLOCK-START:" + blockName + "(.|[\r\n])*BLOCK-END:" + blockName + " -->", 'g'),
        new RegExp("// BLOCK-START:" + blockName + "(.|[\r\n])*// BLOCK-END:" + blockName, 'g'),
        new RegExp("{/\\* BLOCK-START:" + blockName + "(.|[\r\n])*BLOCK-END:" + blockName + " \\*/}", 'g'),
    ];
    findAndReplace({ replaces: replaces, value: value });
};
exports.replaceBlock = replaceBlock;
var replaceBlockWithAlternative = function (_a) {
    var blockName = _a.blockName, alternativeName = _a.alternativeName;
    var alternative = fs
        .readFileSync(path.join('alternatives', alternativeName + ".txt"))
        .toString();
    exports.replaceBlock({ blockName: blockName, value: alternative });
};
exports.replaceBlockWithAlternative = replaceBlockWithAlternative;
var skipBlock = function (_a) {
    var blockName = _a.blockName;
    exports.replaceBlock({ blockName: blockName, value: '' });
};
exports.skipBlock = skipBlock;
var replaceKey = function (_a) {
    var key = _a.key, value = _a.value;
    var replaces = [
        new RegExp("# @@" + key, 'g'),
        new RegExp("// @@" + key, 'g'),
        new RegExp("{/\\* @@" + key + " \\*/}", 'g'),
        new RegExp("<!-- @@" + key + " -->", 'g'),
    ];
    findAndReplace({ replaces: replaces, value: value });
};
exports.replaceKey = replaceKey;
var skipKey = function (_a) {
    var key = _a.key;
    exports.replaceKey({ key: key, value: '' });
};
exports.skipKey = skipKey;
var selectVersion = function (_a) {
    var folderPath = _a.folderPath, baseName = _a.baseName, versions = _a.versions, selectedVersion = _a.selectedVersion;
    versions.forEach(function (version) {
        if (version === selectedVersion) {
            return;
        }
        fs.rmSync(path.join(folderPath, version));
    });
    fs.renameSync(path.join(folderPath, selectedVersion), path.join(folderPath, baseName));
};
exports.selectVersion = selectVersion;
var writeToFile = function (_a) {
    var filePath = _a.filePath, content = _a.content;
    fs.writeFileSync(filePath, content);
};
exports.writeToFile = writeToFile;
