import { info, question, skipBlock } from './utils';

export const setupAuth = async () => {
  const blocks = [
    {
      name: 'Forgotten password',
      key: 'FORGOTTEN',
    },
    {
      name: 'Sign up',
      key: 'SIGN-UP',
    },
  ];

  for (const block of blocks) {
    const keepBlock = await question({
      prompt: `${block.name}? (y/n) [default: y]:`,
    });

    if (keepBlock === 'n') {
      skipBlock({ blockName: block.key });

      info({ message: `${block.name} removed` });
    }
  }
};
