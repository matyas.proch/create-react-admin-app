import { exec, info, question } from './utils';

export const setupNode = async () => {
  const forgottenPassword = await question({
    prompt: 'Install node modules? (y/n) [default: n]:',
  });

  if (forgottenPassword === 'y') {
    info({ message: 'Installing node modules' });

    exec({ command: 'yarn install' });

    info({ message: 'Node modules installed' });
  } else {
    info({ message: 'Node modules skipped' });
  }
};
