import { info, question, replaceBlockWithAlternative } from './utils';

export const setupAccount = async () => {
  const multipleProfilePictures = await question({
    prompt:
      'Should account have multiple profile pictures? (y/n) (default: y):',
  });

  if (multipleProfilePictures === 'n') {
    replaceBlockWithAlternative({
      blockName: 'ACCOUNT-FRAGMENTS',
      alternativeName: 'account.fragments.single-picture',
    });

    replaceBlockWithAlternative({
      blockName: 'GET-PROFILE-PICTURE-PATH',
      alternativeName: 'formatFile.single-picture',
    });

    info({ message: 'Single picture selected' });
  } else {
    info({ message: 'Multiple pictures selected' });
  }
};
