import * as rimraf from 'rimraf';

export const setupCleanup = async () => {
  rimraf.sync('alternatives');
};
