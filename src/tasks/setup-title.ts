import { info, question, replaceKey } from './utils';

export const setupTitle = async () => {
  const appTitle = (await question({ prompt: 'App title:' })) ?? 'App';

  replaceKey({
    key: 'TITLE',
    value: appTitle,
  });

  info({ message: 'App title set' });
};
