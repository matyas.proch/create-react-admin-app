import * as cp from 'child_process';
import * as fs from 'fs';
import * as path from 'path';
import * as readline from 'readline';

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

export const question = async ({
  prompt,
  optional = true,
}: {
  prompt: string;
  optional?: boolean;
}): Promise<string> => {
  return new Promise((resolve) => {
    const ask = () => {
      rl.question(`\n${prompt}\n`, (answer) => {
        if (!optional && !answer) {
          ask();
        } else {
          resolve(answer);
        }
      });
    };

    ask();
  });
};

export const info = ({ message }: { message: string }) => {
  console.log(message);
};

export const exec = ({ command }: { command: string }): void => {
  info({ message: cp.execSync(command).toString() });
};

export const execBatch = ({ commands }: { commands: string[] }): void => {
  commands.forEach((command) => {
    cp.execSync(command);
  });
};

const applyForFile = ({
  folderPath,
  callback,
}: {
  folderPath: string;
  callback: (props: { filePath: string }) => void;
}) => {
  const files = fs.readdirSync(folderPath);

  files.forEach((file) => {
    const stat = fs.lstatSync(path.join(folderPath, file));

    if (stat.isDirectory()) {
      applyForFile({ folderPath: path.join(folderPath, file), callback });

      return;
    }

    if (stat.isFile()) {
      const extension = path.extname(path.join(folderPath, file));

      if (['.png', '.jpg', '.jpeg', '.gif', 'md'].includes(extension)) {
        return;
      }

      callback({ filePath: path.join(folderPath, file) });
    }
  });
};

const findAndReplace = ({
  replaces,
  value,
}: {
  replaces: RegExp[];
  value: string;
}) => {
  const callback = ({ filePath }: { filePath: string }) => {
    const content = fs.readFileSync(filePath).toString();

    const newContent = replaces.reduce(
      (tmpContent, replace) => tmpContent.replace(replace, value),
      content,
    );

    fs.writeFileSync(filePath, newContent);
  };

  applyForFile({ folderPath: '.', callback });
};

export const replaceBlock = ({
  blockName,
  value,
}: {
  blockName: string;
  value: string;
}) => {
  const replaces = [
    new RegExp(
      `# BLOCK-START:${blockName}(.|[\r\n])*# BLOCK-END:${blockName}`,
      'g',
    ),
    new RegExp(
      `<-- BLOCK-START:${blockName}(.|[\r\n])*BLOCK-END:${blockName} -->`,
      'g',
    ),
    new RegExp(
      `// BLOCK-START:${blockName}(.|[\r\n])*// BLOCK-END:${blockName}`,
      'g',
    ),
    new RegExp(
      `{/\\* BLOCK-START:${blockName}(.|[\r\n])*BLOCK-END:${blockName} \\*/}`,
      'g',
    ),
  ];

  findAndReplace({ replaces, value });
};

export const replaceBlockWithAlternative = ({
  blockName,
  alternativeName,
}: {
  blockName: string;
  alternativeName: string;
}) => {
  const alternative = fs
    .readFileSync(path.join('alternatives', `${alternativeName}.txt`))
    .toString();

  replaceBlock({ blockName, value: alternative });
};

export const skipBlock = ({ blockName }: { blockName: string }) => {
  replaceBlock({ blockName, value: '' });
};

export const replaceKey = ({
  key,
  value,
}: {
  key: string;
  value: string;
}): void => {
  const replaces = [
    new RegExp(`# @@${key}`, 'g'),
    new RegExp(`// @@${key}`, 'g'),
    new RegExp(`{/\\* @@${key} \\*/}`, 'g'),
    new RegExp(`<!-- @@${key} -->`, 'g'),
  ];

  findAndReplace({ replaces, value });
};

export const skipKey = ({ key }: { key: string }) => {
  replaceKey({ key, value: '' });
};

export const selectVersion = ({
  folderPath,
  baseName,
  versions,
  selectedVersion,
}: {
  folderPath: string;
  baseName: string;
  versions: string[];
  selectedVersion: string;
}) => {
  versions.forEach((version) => {
    if (version === selectedVersion) {
      return;
    }

    fs.rmSync(path.join(folderPath, version));
  });

  fs.renameSync(
    path.join(folderPath, selectedVersion),
    path.join(folderPath, baseName),
  );
};

export const writeToFile = ({
  filePath,
  content,
}: {
  filePath: string;
  content: string;
}) => {
  fs.writeFileSync(filePath, content);
};
