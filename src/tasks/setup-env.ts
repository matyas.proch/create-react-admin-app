import { question, writeToFile } from './utils';

export const setupEnv = async () => {
  const modes = ['dev', 'prod'];

  for (const mode of modes) {
    const gqlUrl = await question({
      prompt: `[${mode.toUpperCase()}] GQL url:`,
      optional: false,
    });

    const staticUrl = await question({
      prompt: `[${mode.toUpperCase()}] Static url:`,
      optional: false,
    });

    writeToFile({
      filePath: `.env.${mode}`,
      content: [`GQL_URL=${gqlUrl}`, `STATIC_URL=${staticUrl}`].join('\n'),
    });
  }

  const gqlSchemaUrl = await question({
    prompt: `[CODEGEN] GQL Schema url:`,
    optional: false,
  });

  writeToFile({
    filePath: `.env`,
    content: [`GQL_SCHEMA_URL=${gqlSchemaUrl}`].join('\n'),
  });
};
