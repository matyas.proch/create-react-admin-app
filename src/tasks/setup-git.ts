import { execBatch, info, question } from './utils';

export const setupGit = async () => {
  const gitUrl = await question({ prompt: 'Your git url:' });

  if (gitUrl) {
    execBatch({ commands: ['git init', `git remote add origin ${gitUrl}`] });

    info({ message: 'GIT Setup' });
  } else {
    info({ message: 'GIT Skipped' });
  }
};
