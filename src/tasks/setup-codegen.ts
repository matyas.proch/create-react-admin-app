import { exec, info, question } from './utils';

export const setupCodegen = async () => {
  const runCodegen = await question({
    prompt: 'Run codegen? (Make sure GQL schema is live) (y/n) [default: n]:',
  });

  if (runCodegen === 'y') {
    info({ message: 'Executing codegen' });

    exec({ command: 'yarn gql:g' });

    info({ message: 'Codegen executed' });
  } else {
    info({ message: 'Codegen skipped' });
  }
};
