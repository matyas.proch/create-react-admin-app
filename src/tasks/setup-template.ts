import { config } from './config';
import { execBatch, info } from './utils';

export const setupTemplate = async () => {
  info({ message: 'Copying template' });

  execBatch({
    commands: [`git clone -b master ${config.git} .`, 'rm -rf .git'],
  });

  info({ message: 'Template copied' });
};
