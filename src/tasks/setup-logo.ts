import * as fs from 'fs';
import * as path from 'path';
import * as sharp from 'sharp';

import { config } from './config';
import { info, question, replaceBlock } from './utils';

const LOGO_FOLDER = path.join('public', 'static');

export const setupLogo = async () => {
  const logoPath = (
    await question({
      prompt: 'ABSOLUTE path to local logo (svg, jpg, png supported):',
    })
  ).trim();

  if (logoPath) {
    info({ message: 'Preparing logo' });

    const extension = path.extname(logoPath).toLowerCase().trim();
    let logoName;

    if (extension === '.svg') {
      info({ message: 'SVG Detected' });

      logoName = 'logo.svg';

      fs.copyFileSync(logoPath, path.join(LOGO_FOLDER, logoName));
    } else if (extension === '.jpg' || extension === '.jpeg') {
      info({ message: 'JPEG Detected' });

      logoName = 'logo.jpg';

      await sharp(logoPath)
        .resize(config.logoSize, null, {
          withoutEnlargement: true,
        })
        .toFile(path.join(LOGO_FOLDER, logoName));
    } else if (extension === '.png') {
      info({ message: 'PNG Detected' });

      logoName = 'logo.png';

      await sharp(logoPath)
        .resize(config.logoSize, null, {
          withoutEnlargement: true,
        })
        .toFile(path.join(LOGO_FOLDER, logoName));
    } else {
      info({ message: 'Unsupported image type' });

      return;
    }

    replaceBlock({
      blockName: 'LOGO',
      value: `<LogoStyle src="/static/${logoName}" />`,
    });

    info({ message: 'Logo saved' });
  }
};
