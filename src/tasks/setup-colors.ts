import { question, replaceBlock } from './utils';

export const setupColors = async () => {
  const primaryColor = await question({
    prompt: 'Primary color (HEX with #) (#fff):',
    optional: false,
  });

  replaceBlock({
    blockName: 'PRIMARY-COLORS',
    value: `
    const PRIMARY = {
      lighter: '${primaryColor}',
      light: '${primaryColor}',
      main: '${primaryColor}',
      dark: '${primaryColor}',
      darker: '${primaryColor}',
    };
  `,
  });

  const secondaryColor = await question({
    prompt: 'Secondary color (HEX with #) (#fff):',
    optional: false,
  });

  replaceBlock({
    blockName: 'SECONDARY-COLORS',
    value: `
    const SECONDARY = {
      lighter: '${secondaryColor}',
      light: '${secondaryColor}',
      main: '${secondaryColor}',
      dark: '${secondaryColor}',
      darker: '${secondaryColor}',
    };
  `,
  });
};
