import { setupAccount } from './tasks/setup-account';
import { setupAuth } from './tasks/setup-auth';
import { setupCleanup } from './tasks/setup-cleanup';
import { setupCodegen } from './tasks/setup-codegen';
import { setupColors } from './tasks/setup-colors';
import { setupEnv } from './tasks/setup-env';
import { setupGit } from './tasks/setup-git';
import { setupLogo } from './tasks/setup-logo';
import { setupMetadata } from './tasks/setup-metadata';
import { setupNode } from './tasks/setup-node';
import { setupTemplate } from './tasks/setup-template';
import { setupTitle } from './tasks/setup-title';

const setup = async () => {
  await setupTemplate();
  await setupGit();
  await setupTitle();
  await setupLogo();
  await setupMetadata();
  await setupEnv();
  await setupAuth();
  await setupAccount();
  await setupColors();
  await setupNode();
  await setupCodegen();
  await setupCleanup();
};

setup()
  .then(() => {
    process.exit(0);
  })
  .catch(() => {
    process.exit(1);
  });
