## React admin with Minimal

You need to have permissions to access `git@gitlab.com:matyas.proch/react-admin-app.git`

```shell
yarn compile
yarn local-global

create-react-admin-app # inside the project folder
```

## Checklist before creating

1. Logo file (optional)
2. Primary and secondary colors (optional)
3. Git url (optional)
4. GQL url
5. Static url
6. GQL Schema url
7. Title
8. 

## Implemented

- [x] Git init
- [x] Title
- [x] Logo
- [ ] Metadata
- [x] Envs
- [x] Optional forgotten password
- [x] Optional sign up
- [x] Colors
- [x] Node modules
- [x] Codegen
